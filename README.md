[![pipeline status](https://gitlab.com/JohnCharitoudis/git-wrapper/badges/master/build.svg)](https://gitlab.com/JohnCharitoudis/git-wrapper)

# Description

Simple and very easily scalable Git wrapper library for interacting with the Git tool


# Requirements

Only Python 2 and Git tool are required for the program to run. No additional libraries are needed.

# Assumptions

1. Unit tests take into account that they run on a Git repo (in our case the projects repo).

# Usage

To use the library just open a Python interpreter (I suggest you to use a more powerful Python interpreter than the default like [IPython](http://ipython.org/index.html)).

```python

In [1]: from git_wrapper import Git

In [2]: Git.pull()
Out[2]: 'Already up-to-date.\n'

In [3]: Git.push()
Out[3]: 'Everything up-to-date\n'
```

To run the Unit Tests run:
```python
python -m unittest discover -s tests -p 'test_*.py'
```
