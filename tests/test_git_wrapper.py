'''
Created on Nov 13, 2017

@author: johncharitoudis
'''
import unittest
from git_wrapper import *


class Test(unittest.TestCase):


    def test_clone(self):
        result = Git.clone('not_existing_repo')
        expected = 'fatal: repository \'not_existing_repo\' does not exist\n'
        self.assertEqual(result, expected, 'Clone function failed!')
        
        # Check that it raises 'ArgumentsError' in case of not given argument.
        self.assertRaises(ArgumentsError, Git.clone)

    def test_add(self):
        result = Git.add('non_existing_file')
        expected = 'fatal: pathspec \'non_existing_file\' did not match any files'
        self.assertIn(expected, result, 'Add function failed!')

    def test_commit(self):
        result = Git.commit('non_existing_file')
        expected = 'error: pathspec \'non_existing_file\' did not match any file(s) known to git'
        self.assertIn(expected, result, 'Commit function failed!')

    def test_push(self):
        result = Git.push('non_existing_repo')
        expected = 'fatal: \'non_existing_repo\' does not appear to be a git repository'
        self.assertIn(expected, result, 'Push function failed!')

    def test_pull(self):
        result = Git.pull('non_existing_repo')
        expected = 'fatal: \'non_existing_repo\' does not appear to be a git repository'
        self.assertIn(expected, result, 'Pull function failed!')

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_clone']
    unittest.main()