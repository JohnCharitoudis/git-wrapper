'''
Created on Nov 12, 2017

@author: johncharitoudis
'''

import subprocess

class Git(object):
    
    ''' 
    Simple and easily scalable Git wrapper class with API for interaction 
    with Git tool. 
    '''
    
    def __init__(self):
        pass

    @staticmethod
    def clone(*args):
        '''
        Clones a repository into a new directory. Raises an exception if no 
        arguments are given.
        
        Args:
            *args: All valid for Git tool arguments. At least one must be given.
            
        Returns:
            The command output (either stdout or stderr in case of failure) of Git tool.
        '''
        if len(args) == 0:
            msg = 'git-clone takes at least one argument, the repository URL.'
            raise ArgumentsError(msg)
        cmd = 'clone {}'.format(' '.join(args))
        return Git.execute_git_command(cmd)

    # Just to demonstrate how easy is to scale the API.
    @staticmethod
    def add(*args):
        '''
        Adds file contents to the index.
        
        Args:
            *args: All valid for Git tool arguments.
            
        Returns:
            The command output (either stdout or stderr in case of failure) of Git tool.
        '''
        cmd = 'add {}'.format(' '.join(args))
        return Git.execute_git_command(cmd)

    @staticmethod
    def commit(*args):
        '''
        Records changes to the repository
        
        Args:
            *args: All valid for Git tool arguments.
            
        Returns:
            The command output (either stdout or stderr in case of failure) of Git tool.
        '''
        cmd = 'commit {}'.format(' '.join(args))
        return Git.execute_git_command(cmd)
    
    @staticmethod
    def push(*args):
        '''
        Updates remote refs along with associated objects.
        
        Args:
            *args: All valid for Git tool arguments.
            
        Returns:
            The command output (either stdout or stderr in case of failure) of Git tool.
        '''
        cmd = 'push {}'.format(' '.join(args))
        return Git.execute_git_command(cmd)

    @staticmethod
    def pull(*args):
        '''
        Fetches from and integrates with another repository or a local branch.
        
        Args:
            *args: All valid for Git tool arguments.
            
        Returns:
            The command output (either stdout or stderr in case of failure) of Git tool.
        '''
        cmd = 'pull {}'.format(' '.join(args))
        return Git.execute_git_command(cmd)

    # Just to demonstrate how easy is to scale the API.
    @staticmethod
    def status(*args):
        '''
        Shows the working tree status.
        
        Args:
            *args: All valid for Git tool arguments.
            
        Returns:
            The command output (either stdout or stderr in case of failure) of Git tool.
        '''
        cmd = 'status {}'.format(' '.join(args))
        return Git.execute_git_command(cmd)

    @staticmethod
    def execute_git_command(*args):
        '''
        Runs git commands on the host using the Git cli tool.
        
        Args:
            *args: All valid for Git tool arguments.
            
        Returns:
            The command output (either stdout or stderr in case of failure) of Git tool.
        '''
        cmd = 'git {}'.format(' '.join(list(args)))
        try:
            return subprocess.check_output(cmd,
                                          shell=True,
                                          stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            return e.output

class ArgumentsError(Exception):
    pass

